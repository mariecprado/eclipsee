package auxClasses;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebElement;

//import org.openqa.selenium.support.ui.Select;
/*
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Login extends Browsers{
	private String email;
	private String password;
	
	@Test
	public void Ingresar() {
		try {
			email = "xik";
			password = "xik$2015";
			driver.get("http://compras135.ufm.edu/");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.cssSelector("button")).click();
			WebElement findEmail = driver.findElement(By.name("identifier"));
			Actions builder = new Actions(driver);
	        Actions seriesOfActions = builder.moveToElement(findEmail).click().sendKeys(findEmail, email);
	        seriesOfActions.perform();
	        driver.findElement(By.id("identifierNext")).click();
	        ReadUrlFile.Wait(3000);
	        WebElement passw = driver.findElement(By.cssSelector("input[class='whsOnd zHQkBf']"));
	        Actions builderp = new Actions(driver);
	        Actions seriesOfActionsp = builderp.moveToElement(passw).click().sendKeys(passw, password);
	        seriesOfActionsp.perform();
	        driver.findElement(By.id("passwordNext")).click();
	        ReadUrlFile.Wait(5000);

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}