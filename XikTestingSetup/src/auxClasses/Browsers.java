package auxClasses;

import java.io.IOException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

/**
 * Allows the user to change between browsers 
 * @author Jose Wolff, Pablo Miranda, Luis Barrientos
 *
 */

public class Browsers extends ReportClass {
	
	public static WebDriver driver;
	public static WebDriverWait wait;
	public static String url;
	public static WebDriverWait waitingCarousel;
	protected static String currentBrowser;
	/**
	 * Receives as parameters the browser where you want to run the test suite and type if you want to run the browser as mobile or desktop version(default = desktop), this parameter is sent from the testng.xml
	 * firefox 			 - must sent firefox
	 * firefoxMobile 	 - must sent firefox and mobile
	 * safari  		 	 - must sent safari
	 * chrome  		 	 - must sent chrome
	 * chromeMobile		 - must sent chrome and mobile
	 * internet explorer - must sent ie
	 * ieMobile 		 - not available yet.
	 * @param browser
	 * The browser to run the test suite. Firefox type "firefox", Chrome type "chrome" and Internet Explorer type "ie".
	 * Default: firefox.
	 * @param type
	 * Desktop type "desktop" and Mobile type "mobile".
	 * Default: desktop.
	 * @throws IOException
	 * Self explanatory. 
	 */
	@Parameters({"browser","type"})
	@BeforeClass
	public void Init(@Optional("firefox") String browser, @Optional("desktop") String type) throws IOException {
		ReportClass.setIndexPageDescription("Test de regresión para UfmOs");
		currentBrowser = browser;
		//System.out.println("currente " + currentBrowser);
		if(browser.equals("firefox")){
			if(type.equals("desktop")){
				driver = new FirefoxDriver();
				driver.manage().window().maximize();
			}else if(type.equals("mobile")){
				// the user agent string of the mobile we want.
				String userAgent = "Mozilla/5.0 (Lunix; U; Android 4.4.2; HTC One Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.141 Mobile Safari/537.36";
				//we create a profile in firefox.
				FirefoxProfile profile = new FirefoxProfile();
				//Here we change the user agent to mobile.
				profile.setPreference("general.useragent.override", userAgent);
				DesiredCapabilities cap = DesiredCapabilities.firefox();
				cap.setCapability(FirefoxDriver.PROFILE, profile);
				driver = new FirefoxDriver(cap);
				driver.manage().window().maximize();
			}
		}else if(browser.equals("chrome")){
			if(type.equals("desktop")){
				System.setProperty("webdriver.chrome.driver", "drivers/chrome/chromedriver.exe");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions();
			    	options.addArguments("test-type");
			    	options.addArguments("--start-maximized"); 
			    	options.addArguments("--unlimited-storage");
			    	capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				driver = new ChromeDriver(capabilities);
			}else if(type.equals("mobile")){
				//the user agent string of the mobile we want.
				String userAgent  = "Mozilla/5.0 (Lunix; U; Android 4.4.2; HTC One Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.141 Mobile Safari/537.36";
				System.setProperty("webdriver.chrome.driver", "drivers/chrome/chromedriver.exe");
				DesiredCapabilities capabilities = DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions();
			    	options.addArguments("test-type");
			    	options.addArguments("--start-maximized"); 
			    	options.addArguments("--unlimited-storage");
			    	//Here we change the user agent to mobile.
			    	options.addArguments("--user-agent=" + userAgent);
			    	capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				driver = new ChromeDriver(capabilities);
			}
		}else if(browser.equals("ie")){
			System.setProperty("webdriver.ie.driver", "drivers/InternetExplorer/IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
		}else if(browser.equals("safari")){
			driver = new SafariDriver();
		} else if(browser.equals("android")) {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability("device", "Android");
			capabilities.setCapability(CapabilityType.BROWSER_NAME, "browser"); //Name of mobile web browser to automate. Should be an empty string if automating an app instead.
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("automationName", "Appium");
			capabilities.setCapability("deviceName", "HTC One X");
			capabilities.setCapability(CapabilityType.VERSION, "4.2.2");
			capabilities.setCapability(CapabilityType.PLATFORM, "Windows");
			driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		}
		wait = new WebDriverWait(driver,10,1);
		waitingCarousel = new WebDriverWait(driver,10,1);
		url = ReadUrlFile.readUrl();
	}
	
	@AfterClass
	public void CloseDriver(){
		driver.quit(); 
	}
	

}
