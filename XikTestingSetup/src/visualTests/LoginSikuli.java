package visualTests;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class LoginSikuli extends Browsers{
	// Define screen for sikuli.
	Screen screen;	
	
	// Initialize 
	@BeforeClass
	public void init() {
		screen = new Screen();
		
		//Settings.MinSimilarity = 0.95;
	}
	
	
	@BeforeMethod
	public void IngresarSikuli() {
		try {
			
			driver.get("http://compras135.ufm.edu/");
			Region region = screen.wait("img/images/ingresar.png");
			System.out.println("Si lllega 2");
			region.click();
			region.click();
			
			//Email
			region = screen.wait("img/images/emailLogin.png");
			region.click();
			region.type("xik");
			region = screen.wait("img/images/nextEmail.png");
			region.click();
			
			//Password
			region = screen.wait("img/images/passLoginSc.png");
			region.type("xik$2015");
			region = screen.wait("img/images/nextEmail.png");
			region.click();
			
			ReadUrlFile.Wait(4000);
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			AssertJUnit.fail(e.getMessage());
		} catch (Exception e1) {
			AssertJUnit.fail(e1.getMessage());
		}
	}
}	
