package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.Browsers;
import auxClasses.Login;
import auxClasses.ReadUrlFile;
import auxClasses.WindowHandler;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebElement;


//import org.openqa.selenium.support.ui.Select;
/*
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Emulacion extends Browsers{

	@Test
	public void EmulationS() {
		try {
			Login ingresar = new Login();
			ingresar.Ingresar();
			
	        driver.get("http://compras135.ufm.edu/MiU/index.php");
	        driver.findElement(By.linkText("Hacer LOGIN MiU")).click();
	        ReadUrlFile.waitForLoad(driver);
	        driver.findElement(By.id("divFecla_7")).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(text(),'Soporte a estudiantes')]"))).click();
	        ReadUrlFile.waitForLoad(driver);
	        WebElement usuario = driver.findElement(By.xpath("//input[@name='txtSeleccionUsuario']"));
			Actions builderUser = new Actions(driver);
	        Actions seriesOfActionsUser = builderUser.moveToElement(usuario).click().sendKeys(usuario, "20080633");
	        seriesOfActionsUser.perform();
	        ReadUrlFile.waitForLoad(driver);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='ui-id-4']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='bttSearch']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='divFecla_3']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(text(),'Cursos actuales')]"))).click();
	        /*wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='contentTabCursosActuales']/div[5]/table/tbody/tr[23]/td[2]/span"))).click();
	        WindowHandler.next(driver);
	        ReadUrlFile.waitForLoad(driver);*/
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='contentTabCursosActuales']/div[5]/table/tbody/tr[7]/td[1]/span"))).click();
	        WindowHandler.next(driver);
	        ReadUrlFile.waitForLoad(driver);
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='contentTabInformacion']/div[2]/div/div[5]/div/table[2]/tbody/tr[4]/td[2]/span"))).click();
	        WebElement Prueba = driver.findElement(By.xpath("//*[@id='DivContentModalAnuncios']/div[1]/div[3]/input"));
			Actions builderPrueba= new Actions(driver);
	        Actions seriesOfActionsPrueba = builderPrueba.moveToElement(Prueba).click().sendKeys(Prueba, "Prueba");
	        seriesOfActionsPrueba.perform();
	        WebElement fechaInicio = driver.findElement(By.xpath("//*[@id='DivContentModalAnuncios']/div[2]/div[3]/textarea"));
			Actions builderFi= new Actions(driver);
	        Actions seriesOfActionsFi = builderFi.moveToElement(fechaInicio).click().sendKeys(fechaInicio, "Esto Es una Pruba");
	        seriesOfActionsFi.perform();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='bttAceptarAnuncios']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/header/div/div[2]/div[3]/table/tbody/tr[3]/td/div/input"))).click();
	       	ReadUrlFile.Wait(5000);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}