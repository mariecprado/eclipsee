package functionalTests;


import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.Browsers;
import auxClasses.Login;
import auxClasses.ReadUrlFile;

//import org.openqa.selenium.support.ui.Select;
/*
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class LoginPage extends Browsers{
	@Test
	public void Ingresar() {
		try {
			Login ingresar = new Login();
			ingresar.Ingresar();
			ReadUrlFile.Wait(5000);
			driver.findElement(By.cssSelector("img[src='themes/ufm/images/logo-ufm.png']")).click();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}